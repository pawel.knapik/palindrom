package d12;

public class ZadaniePalindrom {

    public static void main(String[] args) {

        String string = "Kobyla ma maly bok";

        System.out.println(isItPalindrom(string));
    }

    private static boolean isItPalindrom(String string) {
        String newString = string.replaceAll("\\s+", "").toLowerCase();

        boolean result = false;
        int minusCounter = -1;

        for (int i = 0; i < newString.length() / 2; i++) {

            if (newString.charAt(i) == newString.charAt(newString.length() + minusCounter)) {
                minusCounter--;
                result = true;
            } else {
                result = false;
                break;
            }
        }
        return result;
    }
}
